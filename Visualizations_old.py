#!/usr/bin/env python
# coding: utf-8

import sys
sys.path.append("/Users/ringelblume/Desktop/Git/EthoVision_Helper")
sys.path.append("/Users/ringelblume/Desktop/Git/Fish_movements")
import numpy as np
#import scipy as sp
#import pandas as pd
#import math
import matplotlib.pyplot as plt
import ev_output_helper as evoh
import fish_movements as fm
import matplotlib.patches as mpatches

rawfilepath = "/Users/ringelblume/Desktop/Dropbox/Project_Robofish/ \
                Gillbach_Data_Set/Spring_DATA_1/"
evoh.data_preprocessing(rawfilepath=rawfilepath,
                        smoothe_all=False,
                        extract_all_unsmoothed=False,
                        subjects_per_trial=1)


dat, framedur = evoh.data_initialization(rawfilepath=rawfilepath,
                                         use_smoothed_data=False,
                                         trial_id=3,
                                         subjects_per_trial=1)


#%% ### Detect individual burst-coast movements

# ### Visualize speed data
plt.clf()
fig = plt.figure(figsize=(10, 5), dpi= 100, facecolor="w", edgecolor="k")
plt.plot(dat.index, "distance_moved_0subj", data=dat, linewidth=2)

bcdat = fm.burst_coast(time=dat["Trialtime"],
                       xseries=dat["X_0subj"],
                       yseries=dat["Y_0subj"],
                       minspeed = 4.5,
                       precision = 0)


#%%
# ### Visualize speed turning points
plt.clf()
fig = plt.figure(figsize=(5, 5), dpi= 100, facecolor="w", edgecolor="k")
plt.plot(bcdat.X_subj, bcdat.Y_subj, c="grey", linewidth=0.5)
plt.title("Burst and coast movements and raw movements")
plt.scatter("X_0subj", "Y_0subj", data=dat, s=10, alpha=0.2, c="pink")
plt.scatter(bcdat.X_subj[bcdat["speed_turningpoints"] == "coast_end"],
            bcdat.Y_subj[bcdat["speed_turningpoints"] == "coast_end"],
            c="b",
            s=1)
plt.scatter(bcdat.X_subj[bcdat["speed_turningpoints"] == "burst_end"],
            bcdat.Y_subj[bcdat["speed_turningpoints"] == "burst_end"],
            c="g",
            s=1)
g_patch = mpatches.Patch(color="g", label="High speed points")
b_patch = mpatches.Patch(color="b", label="Low speed points")
p_patch = mpatches.Patch(color="pink", label="Raw movements")
plt.legend(handles=[g_patch, b_patch, p_patch],
           loc="lower left",
           bbox_to_anchor=(0, -0.2))
plt.savefig("burstcoast.png", dpi=500)


#%%
# ### Plot movements in polar coordinates with 0° axis representing
# direction of previous movement
plt.clf()
fig = plt.figure()
fig = plt.figure(figsize=(9, 8), dpi= 100, facecolor="w", edgecolor="k")
fig.suptitle("Movement directions in relation to previous direction")
ax = fig.add_subplot(221, polar=True)
ax.set_ylim(0, max(bcdat.distance_nextmove))
ax.set_title("Polar coordinates; high speed")
c = ax.scatter(bcdat.dirchange_radians[
                   bcdat["speed_turningpoints"] == "burst_end"],
               bcdat.distance_nextmove[
                   bcdat["speed_turningpoints"] == "burst_end"],
               c="g")
#ax.set_theta_zero_location("N", offset=0)
ax = fig.add_subplot(222, polar=True)
ax.set_ylim(0, max(bcdat.distance_nextmove))
ax.set_title("Polar coordinates; low speed")
c = ax.scatter(bcdat.dirchange_radians[
                   bcdat["speed_turningpoints"] == "coast_end"],
               bcdat.distance_nextmove[
                   bcdat["speed_turningpoints"] == "coast_end"],
               c="b")
#ax.set_theta_zero_location("N", offset=0)
ax = fig.add_subplot(223, polar=False)
ax.set_aspect("equal", adjustable="box")
ax.set_title("Euclidean coordinates; high speed")
Hmap_nextmove, xedg, yedg = np.histogram2d(
    bcdat.Xrel_nextmove[bcdat["speed_turningpoints"] == "burst_end"],
    bcdat.Yrel_nextmove[bcdat["speed_turningpoints"] == "burst_end"],
    bins=80,
    range=[[np.nanmin(bcdat.Xrel_nextmove), np.nanmax(bcdat.Xrel_nextmove)],
           [np.nanmin(bcdat.Yrel_nextmove), np.nanmax(bcdat.Yrel_nextmove)]])
Hmap_nextmove = np.transpose(Hmap_nextmove)
c = ax.imshow(Hmap_nextmove,
              cmap="Blues",
              origin="lower",
              extent=[xedg[0], xedg[-1], yedg[0], yedg[-1]])
ax = fig.add_subplot(224, polar=False)
ax.set_aspect("equal", adjustable="box")
ax.set_title("Euclidean coordinates; low speed")
Hmap_nextmove, xedg, yedg = np.histogram2d(
    bcdat.Xrel_nextmove[bcdat["speed_turningpoints"] == "coast_end"],
    bcdat.Yrel_nextmove[bcdat["speed_turningpoints"] == "coast_end"],
    bins=80,
    range=[[np.nanmin(bcdat.Xrel_nextmove), np.nanmax(bcdat.Xrel_nextmove)],
           [np.nanmin(bcdat.Yrel_nextmove), np.nanmax(bcdat.Yrel_nextmove)]])
Hmap_nextmove = np.transpose(Hmap_nextmove)
c = ax.imshow(Hmap_nextmove,
              cmap="Blues",
              origin="lower",
              extent=[xedg[0], xedg[-1], yedg[0], yedg[-1]])
plt.savefig("relmoves.png", dpi=500)