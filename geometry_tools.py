#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb 22 15:55:38 2023

@author: spathiphyllum
"""

import math
import numpy as np
import scipy.spatial as sp


#%%
def atan2_regularizer(angle:float):
    """ Checks whether a sum or difference between two atan2 angles
    is larger than 180°, in which case it converts it to the equivalent,
    smaller angle of the opposite sign.

        Parameters:
            angle             - raw angle difference or sums in atan2

        Returns:
            angle             - regularized angles in atan2
    """
    import numpy as np

    angle = np.sign(angle) * (abs(angle) % (2*np.pi))

    if angle > np.pi:
        angle = angle - 2*np.pi
    if angle < -np.pi:
        angle = angle + 2*np.pi
    return float(angle)

#%%
def sigmoid_to_atan2(x:float):
    """
    Rescales values between 0 and 1 into values between -pi an +pi.

    Parameters
    ----------
    x : float
        Value to be rescaled.

    Returns
    -------
    float
        Rescaled value.

    """
    y = (2*x - 1) * np.pi
    return float(y)

#%%
def atan2_to_sigmoid(x:float):
    """
    Rescales values between -pi an +pi to values between 0 and 1.

    Parameters
    ----------
    x : float
        Value to be rescaled.

    Returns
    -------
    float
        Rescaled value.

    """
    y = (x + np.pi) / (2 * np.pi)
    return float(y)

#%%
def two_sided_clip(x:float, ceiling:float):
    """
    Clips values to an absolute ceiling, preserving their sign.

    Parameters
    ----------
    x : float
        Value to be clipped.
    ceiling : float
        Absolute ceiling for clipping.

    Returns
    -------
    x : float
        Clipped value.

    """
    if x > ceiling:
        x = ceiling
    if x < -ceiling:
        x = -ceiling
    return x

#%%
def min_max_scaling(series):
    """
    Rescales a series of values to values in [0,1].

    Parameters
    ----------
    series : list or 1D array
        Series to be rescaled

    Returns
    -------
    newseries : list or 1D array
        Rescaled series

    """
    if np.max(series) == np.min(series):
        newseries = [0.5 for i in range(0, len(series))]
    else:
        newseries = ((series - np.min(series))
                     / (np.max(series) - np.min(series)))
    return newseries

#%%
def standardize(series):
    """
    Standardizes a series of values. In the case of no variation, returns zeros.

    Parameters
    ----------
    series : list of 1D array
        Series to be standardized

    Returns
    -------
    newseries : list or 1D array
        Standardizes series

    """
    if np.std(series) == 0:
        newseries = [0.0 for i in range(0, len(series))]
    else:
        newseries = (series - np.mean(series)) / np.std(series)
    return newseries

#%%
def dist_point_line(xcoord:float, ycoord:float, a:float, b:float, c:float):
    """ Computes the distance between a point and a line.

    Parameter:
        xcoord            - x coordinate of the point
        ycoord            - y coordinate of the point
        a, b and c        - parameters of the line function
                            in a*x+b*y+c = 0 form

    Returns:
        linedist          - distance between point and line
    """
    if a == b == 0:
        print("Parameters do not define a line, please check!")
    linedist = abs((a*xcoord + b*ycoord + c)) / math.sqrt(a**2 + b**2)
    return abs(float(linedist))

#%%
def closest_point_on_line(xcoord:float, ycoord:float,
                          a:float, b:float, c:float):
    """ Finds closest point on a line to another point.

    Parameter:
        xcoord            - x coordinate of external point
        ycoord            - y coordinate of external point
        a, b and c        - parameters of the line function
                            in a*x+b*y+c = 0 form

    Returns:
        xclose            - x coordinate of point on line
        yclose            - y coordinate of point on line
    """
    if a == b == 0:
        print("Parameters do not define a line, please check!")
    if a == 0:
        xclose = xcoord
        yclose = -c / b
    elif b == 0:
        xclose = -c / a
        yclose = ycoord
    else:
        xclose = (ycoord - b/a*xcoord + c/b) / (-a/b - b/a)
        yclose = -a/b * xclose - c/b
    return float(xclose), float(yclose)


#%%
def ray_of_vision(xcoord:float, ycoord:float, agent_angle:float,
                  viewing_angle:float):
    """ Computes parameters of a line representing
    an agent's viewing direction in 2D.

    Parameter:
        xcoord            - x coordinate of agent
        ycoord            - y coordinate of agent
        agent_angle       - global heading direction of agent in atan2
        viewing_angle     - viewing angle of agent relative to
                            its heading direction in atan2

    Returns:
        slope             - slope of vision ray
        intercept         - intercept of vision ray
    """
    rayangle = atan2_regularizer(agent_angle + viewing_angle)
    slope = np.sin(rayangle) / np.cos(rayangle)
    intercept = ycoord - slope * xcoord
    return float(slope), float(intercept)

#%%
def intersection(slope1:float, intercept1:float,
                 slope2:float, intercept2:float):
    """ Computes the intersection point between two lines.
    """
    xpoint = (intercept2-intercept1) / (slope1-slope2)
    ypoint = slope1*xpoint + intercept1
    if abs(slope1) > 1000000:
        ypoint = slope2*xpoint + intercept2
    return float(xpoint), float(ypoint)

#%%
def fit_rectangle(points):
    """ Function for fitting the smallest area rectangle that includes
    all points given using the rotating calipers algorithm.

    Arguments:
        points        - series of x and y coordinates of points

    Returns:
        corners       - x and y coordinates of the new rectangle's corners
    """

    # Rotating calipers algorithm

    points = points[~np.isnan(points).any(axis=1), :]
    hull = sp.ConvexHull(points)

    rectangles = []

    for edge in hull.simplices:
        # Find hull vertix furthest from the current edge
        slope = ((points[edge[0],1] - points[edge[1],1])
                 / (points[edge[0],0] - points[edge[1],0]))
        intercept = points[edge[0],1] - slope * points[edge[0],0]
        othervertices = []
        for vertex in hull.vertices:
            newdist = dist_point_line(points[vertex, 0],
                                      points[vertex, 1],
                                      slope,
                                      -1,
                                      intercept)
            oppo = [points[vertex, 0],
                    points[vertex, 1],
                    newdist]
            othervertices.append(oppo)
        othervertices = np.array(othervertices)
        oppositevertex = np.nanargmax(othervertices[:,-1])

        # Infer slope and intercept of opposite side of potential rectangle,
        # and its width
        opposite_slope = slope
        opposite_inter = (othervertices[oppositevertex, 1]
                          - slope * othervertices[oppositevertex,0])
        width = othervertices[oppositevertex, -1]

        # Project all other vertices onto the current edge
        projections = []
        for vertex in hull.vertices:
            projectionx, projectiony = closest_point_on_line(points[vertex, 0],
                                                             points[vertex, 1],
                                                             slope,
                                                             -1,
                                                             intercept)
            projections.append([projectionx, projectiony])
        projections = np.array(projections)

        # Find the two projections furthest away from each other
        # (corners of the potential rectangle)
        startpoint = projections[0, :]
        distance = 0
        for projection in projections:
            newdist = math.sqrt((projection[0] - startpoint[0])**2
                                + (projection[1] - startpoint[1])**2)
            if newdist > distance:
                distance = newdist
                corner1 = projection
        distance = 0
        for projection in projections:
            newdist = math.sqrt((projection[0] - corner1[0])**2
                                + (projection[1] - corner1[1])**2)
            if newdist > distance:
                distance = newdist
                corner2 = projection
        breadth = distance

        # Compute rectangle's area and remaining corners
        area = width * breadth

        corner3 = closest_point_on_line(corner1[0],
                                        corner1[1],
                                        opposite_slope,
                                        -1,
                                        opposite_inter)
        corner4 = closest_point_on_line(corner2[0],
                                        corner2[1],
                                        opposite_slope,
                                        -1,
                                        opposite_inter)

        rectangle = [corner1[0],
                     corner1[1],
                     corner2[0],
                     corner2[1],
                     corner4[0],
                     corner4[1],
                     corner3[0],
                     corner3[1],
                     area]
        rectangles.append(rectangle)

    # Choose smallest rectangle
    rectangles = np.array(rectangles)
    smallest = np.nanargmin(rectangles[:,-1])
    best_rectangle = rectangles[smallest]

    corners = [[best_rectangle[0], best_rectangle[1]],
               [best_rectangle[2], best_rectangle[3]],
               [best_rectangle[4], best_rectangle[5]],
               [best_rectangle[6], best_rectangle[7]]]
    corners = np.array(corners)

    # Compute wall slopes and intercepts
    walls = np.empty([0,2],float)
    walls = np.append(walls,
                      [np.polyfit(corners[0:2,0], corners[0:2,1], 1)],
                      axis=0)
    walls = np.append(walls,
                      [np.polyfit(corners[1:3,0], corners[1:3,1], 1)],
                      axis=0)
    walls = np.append(walls,
                      [np.polyfit(corners[2:4,0], corners[2:4,1], 1)],
                      axis=0)
    walls = np.append(walls,
                      [np.polyfit([corners[3,0], corners[0,0]],
                                  [corners[3,1], corners[0,1]],
                                  1)],
                      axis=0)

    return corners, walls