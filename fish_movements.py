#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created May 2020

@author: %Lea Musiolek

This tool is shared under the GNU General Public License v3.0 or later.
"""
import pandas as pd
import math
import numpy as np

import matplotlib.pyplot as plt
from shapely.geometry.polygon import Polygon

import geometry_tools as geom

#%%
def framewise_velocity(xseries, yseries):
    """ Computes, between each frame and the preceding one,
        the movement vector, its magnitude (the distance moved
                                            from the last frame)
        and its direction relative to the positive x axis.

        Parameters:
        xseries             - Series of x coordinates
        yseries             - Series of y coordinates

        Returns:
        dispx               - Series of displacement values on the x axis
        dispy               - Series of displacement values on the y axis
        magnit              - Series of magnitude values (i. e. distance moved
                                                          since last frame)
        angle               - Series of global movement angles in radians
        (i. e. the absolute angle relative to the positive x axis)
    """

    magnit = list([0])
    angle = list([np.nan])
    dispx = list([0])
    dispy = list([0])
    for i in range(1, len(xseries)):
        # Compute displacement vector in this frame
        disp_x = xseries[i] - xseries[i-1]
        dispx.append(disp_x)
        disp_y = yseries[i] - yseries[i-1]
        dispy.append(disp_y)
        # Compute distance traveled in this frame
        dist = np.sqrt((disp_x)**2 + (disp_y)**2)
        magnit.append(dist)
        # Compute global movement angle in this frame
        ang = math.atan2(disp_y, disp_x)
        angle.append(ang)
    return dispx, dispy, magnit, angle

#%%
def ego_to_global_rotation(vectorx, vectory, agentangle:float):
    """ Takes a vector in egocentric coordinates from the point of view
        of an agent and transforms them into coordinates in the
        global cartesian coordinate system.

        Parameters:
        vectorx, vectory  - Vector to be transformed
        agentangle        - heading angle of the agent

        Returns:
        newvec            - Transformed vector
    """

    R = np.array([[np.cos(agentangle), -np.sin(agentangle)],
                  [np.sin(agentangle), np.cos(agentangle)]])
    vec = [[float(vectorx)],
           [float(vectory)]]
    newvec = R.dot(vec)
    return float(newvec[0]), float(newvec[1])

#%%
def global_to_ego_rotation(vectorx, vectory, agentangle:float):
    """ Takes a vector in global cartesian coordinates and transforms them into
        coordinates in the egocentric cartesian coordinate system
        from the point of view of an agent.

        Parameters:
        vectorx, vectory  - Vector to be transformed
        agentangle        - heading angle of the agent

        Returns:
        newvec            - Transformed vector
    """

    R = np.array([[np.cos(-agentangle), -np.sin(-agentangle)],
                  [np.sin(-agentangle), np.cos(-agentangle)]])
    vec = [[float(vectorx)],
           [float(vectory)]]
    newvec = R.dot(vec)
    return float(newvec[0]), float(newvec[1])

#%%
def burst_coast(time, xseries, yseries, minspeed:float, precision:int):
    """ Takes framewise 2D cartesian coordinate values of fish and returns
    a data frame containing burst-coast segments.

        Parameters:
        time                - Series of time points for coordinates
        xseries             - Series of x coordinates
        yseries             - Series of y coordinates
        minspeed            - minimum speed still to be interpreted as movement
                              (unit: xseries unit / time unit)
        precision           - number of decimal places to round speed to

        Returns:
        dat_tp              - Segmented data frame
    """

    if (len(time) == len(xseries) and
        len(time) == len(yseries)):
        print("")
    else:
        print("Length of data dimensions unequal, please check!")

    dat = pd.DataFrame({"Trialtime": time,
                        "X_subj": xseries,
                        "Y_subj": yseries})

    dat["speed_frame"] = np.nan
    for i in range(1, len(dat)):
        dist = np.sqrt(
            (dat.X_subj[i] - dat.X_subj[i-1])**2
            + (dat.Y_subj[i] - dat.Y_subj[i-1])**2 )
        timelapse = dat.Trialtime[i] - dat.Trialtime[i-1]
        # Round so as to avoid minuscule speed differences later on
        dat.iloc[i, dat.columns.get_loc("speed_frame")] = \
            round(dist / timelapse, precision)

    # Mark slow and fast points, i. e. points at the end of a deceleration
    # and of an acceleration, respectively.

    # First, detect standstill moments (in which the fish moved
    # at less than minspeed) and compute their duration.
    moment_standstill = 0
    dat["speed_turningpoints"] = "0"
    dat["dur_standstill"] = np.nan
    for i in range(1, len(dat)-1):
        if dat.speed_frame[i] < minspeed:
            dat.iloc[i, dat.columns.get_loc("speed_frame")] = 0
            if dat.speed_frame[i-1] >= minspeed:
                moment_standstill = dat.Trialtime[i-1]
            if dat.speed_frame[i+1] >= minspeed:
                end_standstill = dat.Trialtime[i]
                dat.iloc[i, dat.columns.get_loc("dur_standstill")] = \
                    end_standstill - moment_standstill

    # Then, compute accel and decel points more robustly
    for i in range(1, len(dat)-1):
        if (dat.speed_frame[i] <= dat.speed_frame[i-1] and
            dat.speed_frame[i] < dat.speed_frame[i+1]):
            dat.iloc[i, dat.columns.get_loc("speed_turningpoints")] = \
                "coast_end"
        elif (dat.speed_frame[i] > dat.speed_frame[i-1] and
              dat.speed_frame[i] >= dat.speed_frame[i+1]):
            dat.iloc[i, dat.columns.get_loc("speed_turningpoints")] = \
                "burst_end"

    # Create data set with only turning points
    dat_tp = dat.loc[:0]
    dat_tp = dat_tp.append(dat.loc[dat["speed_turningpoints"] != "0"])
    dat_tp.reset_index(inplace=True)
    dat_tp.drop("index", axis=1, inplace=True)

    # Extract movement vectors and distance traveled
    dat_tp["Xdir_nextmove"] = np.nan
    dat_tp["Ydir_nextmove"] = np.nan
    dat_tp["distance_nextmove"] = np.nan
    for i in range(0, len(dat_tp)-1):
        dat_tp.loc[i, "Xdir_nextmove"] = (dat_tp.loc[i+1, "X_subj"]
                                          - dat_tp.loc[i, "X_subj"])
        dat_tp.loc[i, "Ydir_nextmove"] = (dat_tp.loc[i+1, "Y_subj"]
                                          - dat_tp.loc[i, "Y_subj"])
        dat_tp.loc[i, "distance_nextmove"] = \
            np.sqrt(dat_tp.loc[i, "Xdir_nextmove"]**2
                    + dat_tp.loc[i, "Ydir_nextmove"]**2)

    # Extract direction change since last movement
    # Extract x and y coordinates of relative movements
    dat_tp["dirchange_radians"] = np.nan
    dat_tp["Xrel_nextmove"] = np.nan
    dat_tp["Yrel_nextmove"] = np.nan
    for i in range(1, len(dat_tp)):
        dirch = (math.atan2(dat_tp.Ydir_nextmove[i], dat_tp.Xdir_nextmove[i])
                 - math.atan2(dat_tp.Ydir_nextmove[i-1],
                              dat_tp.Xdir_nextmove[i-1]))
        dirch = geom.atan2_regularizer(dirch)
        dat_tp.loc[i, "dirchange_radians"] = dirch

        dat_tp.loc[i, "Xrel_nextmove"] = \
            math.cos(dirch) * dat_tp.distance_nextmove[i]
        dat_tp.loc[i, "Yrel_nextmove"] = \
            math.sin(dirch) * dat_tp.distance_nextmove[i]

    return dat_tp

#%%
def agent_partner_polar(agentx:float, agenty:float, agentang:float,
                        partnerx:float, partnery:float, partnerang:float):
    """ Computes the angle in radians and distance of another individual
        from the point of view of the agent.

        Parameters:
        agentx             - x coordinate
        agenty             - y coordinate
        agentang           - orientation value in radians for the agent
        partnerx           - x coordinate
        partnery           - y coordinate
        partnerang         - orientation value in radians for the partner

        Returns:
        dist               - Distance between agent and non-agent
        fov_angle          - Position of non-agent in the agent"s FOV
                             in radians
    """

    xdist = partnerx - agentx
    ydist = partnery - agenty
    distance = np.sqrt(ydist**2 + xdist**2)

    distangle = math.atan2(ydist, xdist)
    fov_angle = geom.atan2_regularizer(distangle - agentang)

    heading_diff = geom.atan2_regularizer(partnerang - agentang)

    return distance, fov_angle, heading_diff

#%%
def agent_partner_cartesian(agentx:float, agenty:float, agentang:float,
                            partnerx:float, partnery:float):
    """
    Computes the x and y coordinates of a partner in the subjective
    coordinate system of the agent.

        Parameters:
        agentx             - x coordinate
        agenty             - y coordinate
        agentang           - orientation value in radians for the agent
        partnerx           - x coordinate
        partnery           - y coordinate

        Returns:
        relx               - x coordinate of partner in subjective
                             space of agent
        rely               - y coordinate of partner in subjective
                             space of agent
    """

    xdist = partnerx - agentx
    ydist = partnery - agenty

    newx, newy = global_to_ego_rotation(xdist, ydist, agentang)

    return newx, newy

#%%
def find_wallpoints_polar(walls, focalx:float, focaly:float, focalangle:float,
                          field_of_view:float):
    """ Function for computing the closest objective and visible points
    on several straight walls
    from the point of view of an agent, in polar coordinates.

    Arguments:
        walls            List of lists, one for each wall, containing
                         a, b and c parameters of wall line in general form
        focalx           X coordinate of agent in global cartesian space
        focaly           Y coordinate of agent in global cartesian space
        focalangle       Heading angle of agent in global atan2
        field_of_view    Absolute one-sided field of view of agent in atan

    Returns:
        visible_wallpoints    Numpy array with one row per wall,
                              each containing:
                              distance from wall
                              dirchange necessary to swim directly at the wall
                              x coord of closest point on the wall
                              y coord of closest point on the wall
    """

    visible_wallpoints = []
    for wall in walls:
        walldist = geom.dist_point_line(focalx,
                                      focaly,
                                      wall[0],
                                      wall[1],
                                      wall[2])

        xclose, yclose = geom.closest_point_on_line(focalx,
                                      focaly,
                                      wall[0],
                                      wall[1],
                                      wall[2])

        wallangle = math.atan2(yclose - focaly,
                               xclose - focalx)
        wall_heading = geom.atan2_regularizer(wallangle - focalangle)
        objective_wallpoint = [walldist,
                               wall_heading,
                               xclose,
                               yclose]

        if abs(wall_heading) <= field_of_view:
            # objective closest wall point is within FOV
            visible_wallpoint = objective_wallpoint
        elif abs(wall_heading) - field_of_view < np.pi/2:
            # objective closest wall point is outside FOV, but fish can see wall
            rayslope, rayintercept = geom.ray_of_vision(focalx,
                                                        focaly,
                                                        focalangle,
                                                        field_of_view
                                                            * np.sign(
                                                            wall_heading))
            xclose_visi, yclose_visi = geom.intersection(rayslope,
                                                         rayintercept,
                                                         -wall[0] / wall[1],
                                                         -wall[2] / wall[1])
            xray = xclose_visi - focalx
            yray = yclose_visi - focaly
            visible_wallangle = math.atan2(yray, xray)
            visible_wallpoint = [np.sqrt(xray**2 + yray**2),
                                 geom.atan2_regularizer(visible_wallangle
                                                        - focalangle),
                                 xclose_visi,
                                 yclose_visi]
        else: # fish cannot see wall
            visible_wallpoint = [0.0 for a in range(0, 4)]

        visible_wallpoints.append(visible_wallpoint)

    visible_wallpoints = np.array(visible_wallpoints)
    return visible_wallpoints

#%%
def find_wallpoints_cartesian(walls, focalx:float, focaly:float,
                              focalangle:float, field_of_view:float):
    """ Function for computing the closest objective and visible points
    on several straight walls from the point of view of an agent,
    in cartesian coordinates.

    Arguments:
        walls            List of lists, one for each wall, containing
                         a, b and c parameters of wall line in general form
        focalx           X coordinate of agent in global cartesian space
        focaly           Y coordinate of agent in global cartesian space
        focalangle       Heading angle of agent in global atan2
        field_of_view    Absolute one-sided field of view of agent in atan

    Returns:
        visible_wallpoints    Numpy array with one row per wall,
                              each containing:
                              x coordinate of closest wall point
                              in agent's subjective space
                              y coordinate of closest wall point
                              in agent's subjective space
                              x coord of closest point on the wall
                              y coord of closest point on the wall
    """

    visible_wallpoints = []
    for wall in walls:
        xclose, yclose = geom.closest_point_on_line(focalx,
                                                    focaly,
                                                    wall[0],
                                                    wall[1],
                                                    wall[2])

        rely = yclose - focaly
        relx = xclose - focalx
        wallangle = math.atan2(rely, relx)
        wall_heading = geom.atan2_regularizer(wallangle - focalangle)
        wall_rotx, wall_roty = global_to_ego_rotation(relx, rely, focalangle)
        objective_wallpoint = [wall_rotx,
                               wall_roty,
                               xclose,
                               yclose]

        if abs(wall_heading) <= field_of_view:
            # objective closest wall point is within FOV
            visible_wallpoint = objective_wallpoint
        elif abs(wall_heading) - field_of_view >= np.pi/2:
            # fish cannot see wall
            visible_wallpoint = [0.0 for a in range(0, 4)]
        elif abs(wall_heading) - field_of_view < np.pi/2:
            # objective closest wall point is outside FOV, but fish can see wall
            rayslope, rayintercept = geom.ray_of_vision(focalx,
                                                        focaly,
                                                        focalangle,
                                                        field_of_view
                                                            * np.sign(
                                                            wall_heading))
            xclose_visi, yclose_visi = geom.intersection(rayslope,
                                                         rayintercept,
                                                         -wall[0]/wall[1],
                                                         -wall[2]/wall[1])
            xray = xclose_visi - focalx
            yray = yclose_visi - focaly
            visi_wall_rotx, visi_wall_roty = global_to_ego_rotation(xray,
                                                                    yray,
                                                                    focalangle)
            visible_wallpoint = [visi_wall_rotx,
                                 visi_wall_roty,
                                 xclose_visi,
                                 yclose_visi]
        visible_wallpoints.append(visible_wallpoint)

    visible_wallpoints = np.array(visible_wallpoints)
    return visible_wallpoints

#%%
def get_partner_input(
        focalx:float, focaly:float, focalangle:float, field_of_view:float,
        coordinate_system:str, partnerx:float, partnery:float,
        partnerangle:float):
    """
    Computes the relative position of the partner fish from the perspective
    of the focal fish. If it is outside the field of view, returns 0.0.

    Parameters
    ----------
    focalx : float
        Global x coordinate of agent
    focaly : float
        Global y coordinate of agent
    focalangle : float
        Heading angle of agent in atan2
    field_of_view : float
        One-sided field of view of agent in radians
    coordinate_system : string
        "polar" or "cartesian" coordinate encoding
    partnerx : float
        Global x coordinate of partner
    partnery : float
        Global y coordinate of partner
    partnerangle : float
        Heading angle of partner in atan2

    Returns
    -------
    current_input : list
        List containing all chosen inputs for the current frame

    """

    current_input = np.empty(shape=(1,2))

    if coordinate_system == "polar":
        # Compute partner's position and heading from focal fish's POV
        partner_distance, partner_angle, heading_diff = agent_partner_polar(
            focalx,
            focaly,
            focalangle,
            partnerx,
            partnery,
            partnerangle)
        if abs(partner_angle) <= field_of_view:
            current_input[0,0] = partner_distance
            current_input[0,1] = partner_angle
        else:
            current_input[0,0] = 0.0
            current_input[0,1] = 0.0
    if coordinate_system == "cartesian":
        partner_relx, partner_rely = agent_partner_cartesian(
            focalx,
            focaly,
            focalangle,
            partnerx,
            partnery)
        if abs(math.atan2(partner_rely, partner_relx)) <= field_of_view:
            current_input[0,0] = partner_relx
            current_input[0,1] = partner_rely
        else:
            current_input[0,0] = 0.0
            current_input[0,1] = 0.0

    return current_input

#%%
def get_wall_input(focalx, focaly, focalangle, field_of_view,
                   coordinate_system, walls):
    """
    Computes the relative positions of the visible wall points
    from the perspective of the agent.
    Walls outside the field of view are returned with 0.0 values.

    Parameters
    ----------
    focalx : float
        Global x coordinate of agent
    focaly : float
        Global y coordinate of agent
    focalangle : float
        Heading angle of agent in atan2
    field_of_view : float
        One-sided field of view of agent in radians
    coordinate_system : string
        "polar" or "cartesian" coordinate encodingit seems to lead
    walls : list
        List of lists, one for each wall, containing a, b and c parameters
        of wall line in general form

    Returns
    -------
    current_input : list
        List containing all chosen inputs for the current frame

    """

    current_input = np.empty(shape=(1,8))

    if coordinate_system == "polar":
        # Compute closest visible points on walls and distance from them
        visible_wallpoints = find_wallpoints_polar(walls,
                                                   focalx,
                                                   focaly,
                                                   focalangle,
                                                   field_of_view)
        for i in range(0,4):
            viswall = visible_wallpoints[i,:]
            if np.nan in viswall:
                print("Wall array contains nans")
                break
            current_input[0,i*2] = float(viswall[0])
            current_input[0,i*2+1] = float(viswall[1])
    if coordinate_system == "cartesian":
        # Compute closest visible points on walls and their subjective coordinates
        visible_wallpoints = find_wallpoints_cartesian(walls,
                                                      focalx,
                                                      focaly,
                                                      focalangle,
                                                      field_of_view)
        for i in range(0,4):
            viswall = visible_wallpoints[i,:]
            if np.nan in viswall:
                print("Wall array contains nans")
                break
            current_input[0,i*2] = float(viswall[0])
            current_input[0,i*2+1] = float(viswall[1])

    return current_input


#%%
def prediction_to_movement(prediction, coordinate_system, focalangle,
                           fish_range):
    """
    Turns output of simulator model into global x and y coordinates
    for displacement. Clips the movement length to the fish range.

    Parameters
    ----------
    prediction : Array
        Output array of neural network
    coordinate_system : string
        "polar" or "cartesian", coordinate system of current model
    focalangle : float
        Global heading angle of agent in atan2

    Returns
    -------
    dispx : float
        Displacement of agent in the global x direction
    dispy : float
        Displacement of agent in the global y direction
    focalangle : float
        New global heading direction of agent in atan2

    """
    if coordinate_system == "cartesian":
        move_x = prediction[0]
        move_y = prediction[1]
        focalmagnit = np.sqrt(move_x**2 + move_y**2)
        if focalmagnit > fish_range: # clip movement of focal fish
            move_x = move_x * fish_range / focalmagnit
            move_y = move_y * fish_range / focalmagnit
        dispx, dispy = ego_to_global_rotation(move_x,
                                              move_y,
                                              focalangle)
        focalangle = math.atan2(dispy, dispx)
    if coordinate_system == "polar":
        focalmagnit = prediction[0]
        if focalmagnit > fish_range: # clip movement of focal fish
            focalmagnit = fish_range
        dirchange = prediction[1]
        focalangle = geom.atan2_regularizer(focalangle + dirchange)
        dispx = focalmagnit * np.cos(focalangle)
        dispy = focalmagnit * np.sin(focalangle)
    return dispx, dispy, focalangle

#%%
def get_ray_input(focalx, focaly, focalangle, field_of_view,
                  number_of_rays, walls, partners):
    """
    Parameters
    ----------
    focalx : TYPE
        global x coordinate of agent
    focaly : TYPE
        global y coordinate of agent
    focalangle : TYPE
        heading angle of agent in atan2
    field_of_view : TYPE
        one-sided field of view of agent in radians
    number_of_rays : TYPE
        number of rays to use in raycasting
    walls : TYPE
        numpy array with one row per tank wall, containing
        the wall's parameters in general form
    partners : TYPE
        numpy array with one row per partner, containing
        the x and y coordinates and spherical radius of the partner

    Returns
    -------
    ray_wall_values : TYPE
        numpy array with one row per ray,
        with the x and y coordinates of where the forward ray crosses
        the closest wall, and the inverse distance from the agent
    ray_partner_values : TYPE
        numpy array with one row per ray,
        with the x and y coordinates on the ray that are closest
        to the partner, and the inverse distance from the agent

    """
    ray_wall_values = np.empty((0,3), float)
    ray_partner_values = np.empty((0,3), float)
    for ray in range(0, number_of_rays):
        viewing_angle = (ray * (2*field_of_view / (number_of_rays-1))
                         - field_of_view)
        rayslope, rayintercept = geom.ray_of_vision(focalx,
                                                    focaly,
                                                    focalangle,
                                                    viewing_angle)

        thisray_walls = np.empty((0,3), float)
        for wall in walls:
            wallpointx, wallpointy = geom.intersection(rayslope,
                                                       rayintercept,
                                                       -wall[0] / wall[1],
                                                       -wall[2] / wall[1])
            wallvisionvec = [wallpointx-focalx, wallpointy-focaly]
            wallvisiondirec = math.atan2(wallvisionvec[1], wallvisionvec[0])
            if (round(geom.atan2_regularizer(wallvisiondirec - focalangle),1)
                == round(viewing_angle,1)):
                thisray_walls = np.append(thisray_walls,
                                          [[wallpointx, wallpointy,
                                            1/np.sqrt(wallvisionvec[0]**2
                                                      + wallvisionvec[1]**2)]],
                                          axis=0)
        if len(thisray_walls) != 2:
            print("Check")
        visible_wall = np.nanargmax(thisray_walls[:,-1], axis=0)
        ray_wall_values = np.append(ray_wall_values,
                                    [thisray_walls[visible_wall]],
                                    axis=0)

        thisray_partners = np.empty((0,3), float)
        for partner in partners:
            closepointx, closepointy = geom.closest_point_on_line(partner[0],
                                                                  partner[1],
                                                                  rayslope,
                                                                  -1,
                                                                  rayintercept)
            if np.sqrt((closepointx-partner[0])**2
                       + (closepointy-partner[1])**2) <= partner[-1]:
                objvisionvec = [closepointx-focalx, closepointy-focaly]
                objvisiondirec = math.atan2(objvisionvec[1], objvisionvec[0])
                if (round(geom.atan2_regularizer(objvisiondirec
                                                - focalangle), 1)
                    == round(viewing_angle,1)):
                    thisray_partners = np.append(thisray_partners,
                                                 [[closepointx, closepointy,
                                                   1/np.sqrt(
                                                       objvisionvec[0]**2
                                                       + objvisionvec[1]**2)]],
                                                 axis=0)
                else:
                    thisray_partners = np.append(thisray_partners,
                                                 [[np.nan, np.nan, 0]],
                                                 axis=0)
            else:
                thisray_partners = np.append(thisray_partners,
                                             [[np.nan, np.nan, 0]],
                                             axis=0)
        closest_partner = np.nanargmax(thisray_partners[:,-1],
                                       axis=0)
        ray_partner_values = np.append(ray_partner_values,
                                       [thisray_partners[closest_partner]],
                                       axis=0)

    return ray_wall_values, ray_partner_values

#%%
def visualize_raycasting(focalx, focaly, partnerx, partnery,
                         corners, ray_wall_values, ray_partner_values):
    """
    Parameters
    ----------
    focalx : TYPE
        DESCRIPTION.
    focaly : TYPE
        DESCRIPTION.
    partnerx : TYPE
        DESCRIPTION.
    partnery : TYPE
        DESCRIPTION.
    corners : TYPE
        DESCRIPTION.
    ray_wall_values : TYPE
        DESCRIPTION.
    ray_partner_values : TYPE
        DESCRIPTION.

    Returns
    -------
    None.

    """
    plt.close("all")
    fig = plt.figure(figsize=(5, 5), dpi= 100, facecolor="w", edgecolor="k")
    plt.box(on=False)
    plt.plot(corners[:,0],
             corners[:,1],
             c="k",
             lw=0.5)
    plt.scatter(focalx,
                focaly,
                c="b",
                s=5)
    for wallray in ray_wall_values:
        plt.plot([focalx, wallray[0]],
                 [focaly, wallray[1]],
                 c="b",
                 lw=0.5,
                 ls="dotted")
    plt.scatter(partnerx,
                partnery,
                c="g",
                s=5)
    for partnerray in ray_partner_values:
        plt.plot([focalx, partnerray[0]],
                 [focaly, partnerray[1]],
                 c="g",
                 lw=0.5,
                 ls="dashed")
    plt.show()

#%%
def tank_corners_walls(focaltruth, partnercoords):
    """
    Uses recorded fish tracks to infer the rectangular boundaries of the tank.

    Parameters
    ----------
    focaltruth : array
        ? x 2 array containing ? rows of x and y coordinates of first fish
    partnercoords : array
        ? x 2 array containing ? rows of x and y coordinates of second fish

    Raises
    ------
    Exception
        If the inferred hull is not a rectangle, raises exception

    Returns
    -------
    corners : array
        4 x 2 array of x and y coordinates of corners
    walls : array
        4 x 3 array of parameters of wall lines in general form
    tank : Polygon object
        Polygon object for checking if a new point is within the tank boundaries

    """
    # Compute corners of walls based on complete data
    points = np.append(focaltruth,
                       partnercoords,
                       axis=0)
    corners, walls_SI = geom.fit_rectangle(points)
    tank = Polygon(corners[0:4])

    if round(walls_SI[0,0], 3) != round(walls_SI[2,0], 3):
        raise Exception("Opposite walls do not seem to have the same slope!")
    elif round(walls_SI[1,0], 3) != round(walls_SI[3,0], 3):
        raise Exception("Opposite walls do not seem to have the same slope!")

    walls = []
    for wall in walls_SI:
        walls.append([wall[0], -1, wall[1]])
    walls = np.array(walls)

    plt.close("all")
    fig = plt.figure(figsize=(5, 5),
                     dpi= 100,
                     facecolor="w",
                     edgecolor="k")
    plt.box(on=False)
    plt.title("Automatically fitted walls")
    plt.plot(corners[:,0],
             corners[:,1],
             c="k",
             lw=2)
    plt.scatter(points[:,0],
                points[:,1],
                c="b",
                s=0.1)
    plt.show()

    return corners, walls, tank