# Fish_movements

This is a collection of tools for analyzing 2D tracked movements of animals, more specifically fish. This includes segmenting movement trajectories into burst and coast vectors, extracting direction changes between movements and positions of one fish in the field of view of another.


**ATTENTION**: Does not check for gaps in data, these will be interpreted as movements!!

## Typical use   
```
# Tell python where to find fish_movements.py:

import sys   
sys.path.append("Location/of/fish_movements")     
import fish_movements as fm 

# Segment a subject's coordinates into a sequence of burst and coast movements:

dat_tp = fm.burst_coast(time, xseries, yseries, minspeed, precision)

# Transform a subject's global coordinates into angle and distance from the perspective of an agent:

dist, fov_angle = fm.agent_perspective(agentx, agenty, agentang, nonagentx, nonagenty)


```


## Overview of functions:

**burst_coast:** This function segments 2D movement tracks (e. g. fish movements) into burst and coast movements based on maxima and minima of speed, similarly to Calovi et al. (2018). This reduces data size and enables further analyses based on the physical laws governing some animal species' movements.
It takes series of frame time, x and y coordinates, computes speed of animal movement in each frame, sets any speed below a certain (user-defined) threshold to 0, then detects minima and maxima of speed. Minima are interpreted as ends of coast/glide movements, while maxima are interpreted as ends of burst movements.
Returns a data frame containing only speed minima and maxima, i. e. the segmented trajectories.
Segmenting can be refined through the precision parameter which is the number of decimal places for rounding speed before computing minima and maxima.

**framewise_velocity:** Computes, between each frame and the preceding one, the movement vector, its magnitude (the distance moved from the last frame) and its direction relative to the positive x axis.

**agent_perspective:** uses series of global x and y coordinates and orientation angles of an agent, as well as global x and y coordinates (e. g. of another tracked individual) to compute the latter object's distance from the agent as well as its position as an angle in the agent's field of view.

*This tool is shared under the GNU General Public License v3.0 or later.*
